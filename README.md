# Lab9 -- Security check


## Introduction

You will or you already have been creating applications that are rather sensitive to security so you want to check that your application is secure enough. And maybe you think that you are not competent enough for security testing or maybe the cost of failure is rather big in your case, but usually you want to delegate this work to the other institution to let them say whether the system is secure enough. And in this lab, you would be that institution. ***Let's roll!***

## OWASP

[OWASP](https://owasp.org/) is Open Web Application Security Project. It is noncommercial organization that works to improve the security of software. It creates several products and tools, which we will use today and I hope you will at least take into account in future.

## OWASP Top 10

[OWASP Top 10](https://owasp.org/www-project-top-ten/) is a project that highlights 10 current biggest risks in software security. Highlighting that they make it visible. And forewarned is forearmed. As a software developer, you may at least consider that risks.

## OWASP Cheat Sheet Series

[OWASP CSS](https://cheatsheetseries.owasp.org/) is a project that accumulates the information about security splitted into very specific topics. And sometimes that might answer the question "How to make it secure?" according to specific topics (e.g. forgot password functionality).

## Google security check

Let's check how Google responds to the OWASP Cheatsheets of Forgot Password topic.

### Select the requirement from cheatsheets

E.g. how Google provides the forgot password feature and whether it "Return a consistent message for both existent and non-existent accounts."([link](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html#:~:text=forgot%20password%20service%3A-,Return%20a%20consistent%20message%20for%20both%20existent%20and%20non%2Dexistent%20accounts.,-Ensure%20that%20the)). To check this we should only try to reset password for existent and unexistent user and compare result.

Note: _In your work is important to make link to particlular requirement in cheatsheet._

### Create and execute test case

Up to this moment we do not know what would be during testing forgot password feature at google.com. Not only real result, but even steps. But we can expect some steps in this process and some expected result:
1. Beforehand we have to know some existent account. 
2. Open google.com page
3. Try to authenticate
4. Understand that we forgot the password
5. Try to reset password for some unexistent account
6. Remember the result
7. Go back and make the same for the prepared account
8. Remember the result 
9. Expect that results for 2 users would be indistinguishable, or the intruder might process [user enumeration atack](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html#:~:text=user%20enumeration%20attack)

Note: _You should not provide this high level steps, you might take it into account and go to next step to documenting test case execution_

Now lets try to execute that with documenting each step, to let yourself or someone another to reproduce it. During execution some unexpected issues might happen. It is ok, if the planned test case is not changd a lot just mention it into result section. Or if original plan changes a lot then return to it, review it, change if needed and continue.

| Test step  | Result |
|---|---|
| Open google.com  | Ok |
| Push sign in button  | Login page opened. If you have already logged in log out and repeate  |
| Push forgot email  |  There is no forgot password, but we can push forgot email. It still starts password recovery |
| Open new tab with sign in page  |  I tried to input unexistent account and understand that i do not know whether the account exists. So I would try to register with new account without submiting the form, so i would be sure about email that does not exists |
| Push create account button  |  Ok |
| Push create for yourself tab  |  Ok, create account form opened |
| Input your existent account email into email field  |  Ok, field highlight red |
| Try to change this email to make it unique  |  Ok, also you can input some name and page offer you new unique email. Remember that email |
| Return to forgot password page and input the unexistent email and push submit  |  Ok, form with first and last names opened |
| Input some first and last names and push submit |  Since you do not know the names of unexistent account it is ok to imagine somehting |
| See the result that there is no such an account  |  Ok, remember this result |
| Push retry button  |  Ok, we in the start of forgot password process |
| Input prepared existent email and push submit |  Ok, form with last and first name opened |
| Input first and last names and push submit  |  Ok, since it is prepared account you know its first and last names |
| See the page with confirmation  to send resetting email |  Ok. Remember this result |

Thats all, we tried 2 accounts and get 2 results, they are different so test case failed

### Analysis

Test case was failed, and according to different result intruders might process user enumeration attack and get database with existing users emails names and surnames, at least there is such an opportunity. Of course to brute force every email it require a lot of resources, but in this case it requires much more, because with email intruder need to pick up its name and surname. So even test case failed, that is not an security issue in given context.

Interesting thing that for my account, the email with resetting information would be sent on the same email(that i want to restore password)

Note: _Do not consider this result analysis as guide to action. Just see the test case result and provide your thoughts about_

## Homework

### Goal

To get familiar with OWASP CSS project by practicing limited security blackbox testing based on its materials. The default topics for testing is "Forgot password" and "Files Upload", but we are appeciate you to read another topics and select interesting one.

Note: _You are free to select topics or even combine topics, but remember that you should be able to test it using black box testing_

### Task

1. Pick a website that you can test using blackbox mode and put it into your solution and check that it is unique with previous submissions(web site and topic tested should be unique toghether, e.g. google.com might be tested on forgot password in one submission, and on file upload in another one). Like it was during [previous lab](https://gitlab.com/sqr-inno/s23-lab8-exploratory-testing-and-ui-testing)(If it is hard for you to find such website just google "Forgot password" and you will see list of forgot password pages, just select one for yourself, or if you select another topic, then google it to find websites and pages with another features)
2. Find specific advice/requirement/standard or similar that you want to check whether your picked website follows it. By default look into ["Forgot password"](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html) and ["File upload"](https://cheatsheetseries.owasp.org/cheatsheets/File_Upload_Cheat_Sheet.html) topics, because there are very straight forward checklists, with possible black box testing. 

Note: _It is not mandatory to test Forgot Password feature, moreover it is strongly recommended to go through cheat sheet and find interesting features to test. Lab guide you for Forgot Password feature, only because I want to let every student to pass this lab. You are free(And I appreciate that) to select any topics (Be sure that you are able to ensure that during black box testing)_

3. For entity from previous stage imagine the flow of test(you do not need to document it, but you still can do it for more realism), its inputs, high level steps, and expected results.
4. Follow that flow with documenting each step and results
5. Comment the results of test cases. Is that everything ok, maybe it is a found vulnerability or weakness, or it might be intended weaking of the security protection in given context, what might be an alternative solution.(This is not a checklist that you must follow, just take it into account as line of thought)
6. Repeat from the first point(actually the website might be the same with different topic or entity from second point, and you do not need to mention web site each time) 4 times(the one from the lab is not counted). 

### Notes
1. Test cases might be executed fully manually, using helper tools, or fully automated, but in this cases I prefer to see the source code or know the used tool.
2. I want to see the linkage between entities from points 2,3,4,5 and with the referenced place from OWASP CSS(hope the structure would not change between your submission and my evaluation)
3. Some advices might be easy to test(e.g. password form should contain minimum password length validation), but some of them might require a lof of efforts(e.g. Ensure that the time taken for the user response message is uniform for existent user and unexistent). So 4 tests is just a recomendation it might be higher or lower. Finnaly I can reject submission if see too less effort put into the work. So to write me directly to prereview selected topics and proposed flows might be good idea(but not mandatory).


## Homework Solution

my website is linkedin.com

### Test Forget Password

https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html#forgot-password-cheat-sheet

**Steps:**
1. Have an active account at https://www.linkedin.com
2. Visit https://www.linkedin.com/checkpoint/rp/request-password-reset-submit with your existing account, input your email, and click "Submit"
3. Note the response time from Devtools and the server response for the existing account
4. Access https://www.linkedin.com/checkpoint/rp/request-password-reset-submit with a non-existing account, enter an email, and hit "Submit"
5. Observe the response time from Devtools and the server response for the non-existing account
6. Compare the findings. If the response time is different, it might be an indication that the account exists. If the response is different, it might be an indication that the account does not exist.

| Test step  | Result |
|---|---|
| Visit https://www.linkedin.com | Ok |
| Click "Sign in" button  | Sign-in page opened at https://www.linkedin.com/login?fromSignIn=true&trk=guest_homepage-basic_nav-header-signin  |
| Click on "Forgot password?" |  Opened Forgot Password Page at https://www.linkedin.com/checkpoint/rp/request-password-reset  |
| Enter email for existing account and click "Submit"  |  Ok |
| Check response time for password reset request for existing account (from DevTools) | 300ms |
| Note server response for existing account	| |
| Enter email for non-existing account and click "Submit" | Ok |
| Check response time for password reset request for non-existing account (from DevTools) | 335ms |
| Note server response for non-existing account | |

**Summary:** LinkedIn follows the rules outlined in the OWASP Forgot Password Cheat Sheet for both existing and non-existing accounts, as the response message and response time are roughly the same. This helps prevent attackers from determining which accounts exist on the platform by analyzing the timing. The test results indicate that LinkedIn successfully passed the Forgot Password Cheat Sheet test.

### Test UserIDs

https://cheatsheetseries.owasp.org/cheatsheets/Authentication_Cheat_Sheet.html#user-ids

Plan:

1. Have an existing account at https://www.linkedin.com or create a new one
2. Go to https://www.linkedin.com/login and enter your username with different cases than you originally typed
3. The system should let you in
4. Test step
5. Result

| Test step  | Result |
|---|---|
| Visit https://www.linkedin.com | Done |
| Click "Sign in" button | Sign-in page https://www.linkedin.com/login opened |
| Enter your username with different cases from what you originally typed | With case-changes typed username Aladdinych system logs in, original username was aladdinych

**Summary:**

From the cheatsheets:
"Usernames/user IDs are case-insensitive. User 'smith' and user 'Smith' should be the same user. Usernames should also be unique." LinkedIn follows this rule and passes the Authentication Cheat Sheet user ID test.

### Test Email Address Validation

https://cheatsheetseries.owasp.org/cheatsheets/Input_Validation_Cheat_Sheet.html#email-address-validation

| Test step  | Result |
|---|---|
| Visit https://www.linkedin.com | Done |
| Click "Join now" button | Join-now page opened |
| Fill out all required fields excluding email | Ok |
| Enter email field with more than one "@" | ![](imgs/3.png) |
| Enter email field with less than one "@" | ![](imgs/2.png) |
| Enter email field with more than 254 characters | ![](imgs/4.png) |
| Enter email local part with more than 63 characters | ![](imgs/7.png) |
| Enter email in domain part using characters other than letters, numbers, hyphens, and periods	| Email validation fails but it doesn't show any error message in page, but in console we can see it fails: ![](imgs/8.png) ![](imgs/9.png) |
| Enter correct email | ![](imgs/10.png) |

When validation succeeds, we go to the next page: ![](imgs/11.png)

**Summary:**

Notice, that `gmailgmailgmailgmailgmgmailgmailailgmailgmailgmailgmailgmailgmailgmailgmailgmailgmail@gmail.com` is allowed on linked in and is a valid email address even though it's 85 chars longs in the local part. So that means it fails the email validation test according to the cheatsheets.

Also, the email validation fails when we use characters other than letters, numbers, hyphens, and periods in the domain part, but it doesn't show any error message in page, but in console we can see it fails.

Also, in the "Enter email field with more than 254 chars" part we can see that on LinkedIn it's between 3 and 128 chars longs, so it again fails the email validation test according to the cheatsheets?

### Test Security Response Headers

https://cheatsheetseries.owasp.org/cheatsheets/HTTP_Headers_Cheat_Sheet.html

| Test Step | Result |
|---|---|
| Visit https://www.linkedin.com | Ok |
| Open Dev Tools to see the network panel | Ok |
| Navigate to https://www.linkedin.com/login | Ok |
| Check security header X-Frame-Options | `x-frame-options: sameorigin` |
| Check security header X-XSS-Protection | |
| Check security header X-Content-Type-Options | `nosniff` |
| Check security header Referrer-Policy | |
| Check security header Content-Type | `text/html; charset=utf-8` |
| Check security header Set-Cookie | |

These are the full list of headers:
![](imgs/12.png)

**Summary:**

So, LinkedIn's got a bunch of the recommended security headers like X-Frame-Options, X-Content-Type-Options, Content-Type, and Set-Cookie. But, it's missing the X-XSS-Protection and Referrer-Policy headers. Even though LinkedIn doesn't follow all the tips from the OWASP HTTP Headers Cheat Sheet, it's still got a decent amount of security stuff going on to keep users and their data safe.